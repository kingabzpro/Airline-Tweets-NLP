FROM python:3.9
EXPOSE 8501
WORKDIR /app
COPY requirements.txt ./requirements.txt
RUN pip3 install -r requirements.txt
COPY . .
CMD SET EAI_USERNAME=$YOUR_USER
CMD SET EAI_PASSWORD=$YOUR_PASSWORD
CMD streamlit run --server.port $PORT app.py

